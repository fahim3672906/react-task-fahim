import React, { Component } from "react";

import CheckIcon from "@mui/icons-material/Check";
import ToggleButton from "@mui/material/ToggleButton";
import Button from "@mui/material/Button";
import { TextField, Typography } from "@mui/material";

const AddNew = () => {
  const [selected, setSelected] = React.useState(false);

  return <div>
    <div className="books-container">
      <TextField
        id="outlined-basic"
        label="Title"
        variant="outlined"
        margin="normal"
        fullWidth={true}
      />
      <TextField
        id="outlined-basic"
        label="Author"
        variant="outlined"
        margin="normal"
        fullWidth={true}
      />
      <TextField
        id="outlined-basic"
        label="Language"
        variant="outlined"
        margin="normal"
        fullWidth={true}
      />
      <TextField
        id="outlined-basic"
        label="Country"
        variant="outlined"
        margin="normal"
        fullWidth={true}
      />
      <ToggleButton
        value="check"
        selected={selected}
        onChange={() => {
          setSelected(!selected);
        }}
      >
        <Typography>Available For Borrowing</Typography>
      </ToggleButton>
    </div>
    <div className="button-container">
      <Button variant="contained" fullWidth={true}>
        Add New Book
      </Button>
    </div>
  </div>
}

export default AddNew;