import React from 'react';
import Header from '../header/header';

const Layer = ({children}) => {
    return (
    <div>
        <header className="library-header">
            <Header />
        </header>
        {children}
    </div>);
}

export default Layer;