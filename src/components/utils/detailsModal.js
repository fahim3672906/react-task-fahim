import * as React from "react";
import Box from "@mui/material/Box";
import { Modal, Typography, Grid, Paper } from "@mui/material";
import { styled } from "@mui/material/styles";

const CardContent = styled("div")(() => ({
    display: "flex",
    flexDirection: "column",
    alignContent: "center",
}));

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 8,
};

const DetailsModal = ({ open, handleClose, objToView }) => {
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description">
            <Grid container spacing={2} justifyContent="center">
                <Paper>
                    <Box sx={style}>

                        <Typography variant="h4">
                            Title: {objToView?.title}
                        </Typography>
                        <CardContent>
                            <Typography variant="h6">
                                Author/Director: {objToView?.author || objToView?.director || objToView?.publishedBy}
                            </Typography>
                            <Typography variant="h6">
                                Country: {objToView?.country}
                            </Typography>
                            <Typography variant="h6">
                                Language: {objToView?.language}
                            </Typography>
                            <Typography variant="h6">
                                Available: {objToView?.availableForBorrowing ? "Yes" : "No"}
                            </Typography>
                        </CardContent>
                    </Box>
                </Paper>
            </Grid>
        </Modal>);
}

export default DetailsModal;