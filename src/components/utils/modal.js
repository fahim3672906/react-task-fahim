import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import {
  TextField,
  Modal,
  Typography,
  Checkbox,
  FormControlLabel,
} from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const InputModal = ({
  open,
  handleClose,
  obj,
  handleInputChange,
  handleAvailableToggle,
  handleSubmit,
  isEditing,
  error,
  value,
  modalTitle,
}) => {
  return (
    <div>
      <div className="books-block">
        <div>
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style}>
              <div>
                <div className="books-container">
                  {modalTitle && (
                    <Typography variant="h5" component="h1">
                      {modalTitle}
                    </Typography>
                  )}
                  <TextField
                    error={error.title ? true : false}
                    id={
                      !error.title
                        ? "outlined-basic"
                        : "outlined-error-helper-text"
                    }
                    label={!error.title ? "Title" : "Error"}
                    helperText={!error.title ? null : "Incorrect Entry!"}
                    name="title"
                    variant="outlined"
                    margin="normal"
                    fullWidth={true}
                    value={obj.title || ""}
                    onChange={(event) =>
                      handleInputChange(event.target.name, event.target.value)
                    }
                  />
                  <TextField
                    error={error.author ? true : false}
                    id="outlined-basic"
                    label="Author"
                    name="author"
                    variant="outlined"
                    margin="normal"
                    fullWidth={true}
                    value={obj.author || ""}
                    onChange={(event) =>
                      handleInputChange(event.target.name, event.target.value)
                    }
                  />
                  {!value == "3" && (
                    <TextField
                      error={error.author ? true : false}
                      id={
                        !error.author
                          ? "outlined-basic"
                          : "outlined-error-helper-text"
                      }
                      label={!error.author ? "Author/Director" : "Error"}
                      helperText={!error.author ? null : "Incorrect Entry!"}
                      name="author"
                      variant="outlined"
                      margin="normal"
                      fullWidth={true}
                      value={obj.author ? obj.author || "" : obj.director || ""}
                      onChange={(event) =>
                        handleInputChange(event.target.name, event.target.value)
                      }
                    />
                  )}
                  <TextField
                    error={error.language ? true : false}
                    id="outlined-basic"
                    label="Language"
                    name="language"
                    variant="outlined"
                    margin="normal"
                    fullWidth={true}
                    value={obj.language || ""}
                    onChange={(event) =>
                      handleInputChange(event.target.name, event.target.value)
                    }
                  />

                  <TextField
                    error={error.country ? true : false}
                    id="outlined-basic"
                    label="Country"
                    name="country"
                    variant="outlined"
                    margin="normal"
                    fullWidth={true}
                    value={obj.country || ""}
                    onChange={(event) =>
                      handleInputChange(event.target.name, event.target.value)
                    }
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={obj?.availableForBorrowing ?? false}
                        name="availableForBorrowing"
                        onChange={(e) => handleAvailableToggle(e)}
                      />
                    }
                    label="Available For Borrowing"
                  />
                </div>
                <div className="button-container">
                  <Button
                    variant="contained"
                    fullWidth={true}
                    onClick={handleSubmit}
                  >
                    {isEditing ? "Edit" : "Add New"}
                  </Button>
                </div>
              </div>
            </Box>
          </Modal>
        </div>
      </div>
    </div>
  );
};

export default InputModal;
