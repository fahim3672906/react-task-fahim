import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button } from '@mui/material';

export default function BasicTable(props) {
  const { data, handleDelete, handleEdit, handleView } = props;

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="left">Title</TableCell>
            <TableCell align="left">Author/Director</TableCell>
            <TableCell align="left">Country</TableCell>
            <TableCell align="left">Language</TableCell>
            <TableCell align="left">Borrowing</TableCell>
            <TableCell align="left">Action</TableCell>
            <TableCell align="left">Action</TableCell>
            <TableCell align="left">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data?.map((book) => {
            const { author, director, country, language, availableForBorrowing, title, id, publishedBy } = book;
            return <TableRow
            key={id}
          >
            <TableCell component="th" scope="row">
            {title}
            </TableCell>
            <TableCell component="th" scope="row">
            { author || director || publishedBy}
            </TableCell>
            <TableCell component="th" scope="row">
            {country}
            </TableCell>
            <TableCell component="th" scope="row">
            {language}
            </TableCell>
            <TableCell align="left">{availableForBorrowing ? "Yes" : "No"}</TableCell>
            <TableCell align="left">
              <Button onClick={() => handleView(id)}>View</Button>
            </TableCell>
            <TableCell align="left">
              <Button onClick={() => handleEdit(id)}>Edit</Button>
            </TableCell>
            <TableCell align="left"><Button onClick={() => handleDelete(id)}>Delete</Button></TableCell>
          </TableRow>
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
