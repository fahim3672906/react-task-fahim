import React from "react";
import Layer from "../layer";
import "../library.scss";
import Books from "../../pages/books";

const Library = () => {
  return (
    <Layer>
      <div className="library-content">
        <div className="tabs-container">
        <Books />
        </div>
      </div>
    </Layer>
  );
};

export default Library;
