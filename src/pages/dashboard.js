import React from "react";
import { useNavigate } from "react-router-dom";
import Grid from "@mui/material/Grid";
import CategoryCard from "../components/CategoryCard";
import BookRoundedIcon from "@mui/icons-material/BookRounded";
import NewspaperRoundedIcon from "@mui/icons-material/NewspaperRounded";
import HistoryEduRoundedIcon from "@mui/icons-material/HistoryEduRounded";
import Layer from "../components/layer";
import { Container } from "@mui/material";

const Dashboard = () => {
  const navigate = useNavigate();

  return (
    <Layer>
      <Container maxWidth="lg">
        <Grid
          container
          spacing={3}
          sx={{
            mt: 10,
          }}
        >
          <Grid item xs={12} md={6}>
            <CategoryCard
              title="Books"
              Icon={BookRoundedIcon}
              handleClick={() => navigate("/books")}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <CategoryCard
              title="Newspapers"
              Icon={NewspaperRoundedIcon}
              handleClick={() => navigate("/newspapers")}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <CategoryCard
              title="Documentaries"
              Icon={HistoryEduRoundedIcon}
              handleClick={() => navigate("/documentaries")}
            />
          </Grid>
        </Grid>
      </Container>
    </Layer>
  );
};

export default Dashboard;
