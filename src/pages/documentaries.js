import * as React from "react";
import Button from "@mui/material/Button";
import { TextField, Pagination } from "@mui/material";
import BasicTable from "../components/table/basicTable";
import { DocumentariesData } from "../constants/mockData/data";
import InputModal from "../components/utils/modal";
import { validateName } from "../helperfunctions/validations";
import Layer from "../components/layer";
import DetailsModal from "../components/utils/detailsModal";
import { rootContext } from "../context";

const Documentaries = () => {
  const documentatiationData = React.useContext(rootContext);
  const [documentariesData, setDocumentariesData] =
    React.useState(DocumentariesData);
  const [open, setOpen] = React.useState(false);
  const [openModal, setOpenModal] = React.useState(false);
  const [isEditing, setIsEditing] = React.useState(false);
  const [bookObj, setBookObj] = React.useState({});
  const [objToView, setObjToView] = React.useState({});
  const [page, setPage] = React.useState(1);
  const [error, setError] = React.useState({
    title: false,
    author: false,
    director: false,
    publishedBy: false,
    language: false,
    country: false,
    year: false,
  });

  /* Filter handling */
  const handleFilter = (e) => {
    if (e) {
      const newData = documentariesData?.filter((book) =>
        book?.title
          .toString()
          ?.toLowerCase()
          ?.startsWith(e?.toString()?.toLowerCase())
      );
      // setDocumentariesData(newData);
      documentatiationData.setDocumentaionDetails(newData);

      setPage(1);
    } else {
      setDocumentariesData(DocumentariesData);
    }
  };

  /* Form handling */
  const handleInputChange = (name, value) => {
    const data = validateName(value);
    setError((prevState) => ({
      ...prevState,
      [name]: data ? true : false,
    }));
    setBookObj((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = () => {
    if (!isEditing) {
      bookObj.id = documentariesData.length + 1;
      const documentaries = [...documentariesData, bookObj];
      const err = Object.values(error);

      if (err.every((er) => er == false)) {
        setDocumentariesData(documentaries);
        documentatiationData.setDocumentaionDetails(documentaries);
        setBookObj({});
        setOpen(false);
      } else {
        return null;
      }
    } else {
      const documentaries = documentariesData?.map((doc) =>
        doc.id === bookObj.id ? bookObj : doc
      );
      const err = Object.values(error);

      if (err.every((er) => er == false)) {
        setDocumentariesData(documentaries);
        documentatiationData.setDocumentaionDetails(documentaries);

        setBookObj({});
        setOpen(false);
      } else {
        return null;
      }
    }
  };

  const handleEdit = (id) => {
    setOpen(true);
    setIsEditing(true);
    const docObj = documentariesData?.filter((bookObj) => bookObj.id === id);
    setBookObj(docObj[0]);
  };

  const handleAvailableToggle = (e) =>
    setBookObj((prevState) => ({
      ...prevState,
      availableForBorrowing: !bookObj.availableForBorrowing,
    }));

  const handleDelete = (id) => {
    const newDocumentaries = documentariesData?.filter(
      (book) => book?.id !== id
    );
    setDocumentariesData(newDocumentaries);
    documentatiationData.setDocumentaionDetails(newDocumentaries);
  };

  /* Modal handling */
  const handleView = (id) => {
    const docObj = documentariesData?.filter((bookObj) => bookObj.id === id);
    setObjToView(docObj[0]);
    setOpenModal(true);
  };

  const handleModalClose = () => {
    setOpenModal(false);
  };

  const handleOpen = () => {
    setIsEditing(false);
    setBookObj({});
    setError({});
    setOpen(!open);
  };

  const handleClose = () => {
    setOpen(!open);
    setIsEditing(false);
  };

  /* Pagination handling */

  const handlePageChange = (event, value) => {
    setPage(value);
  };

  return (
    <Layer>
      <div className="library-content">
        <div className="tabs-container">
          <>
            <InputModal
              modalTitle={"Add new Documentary"}
              open={open}
              handleClose={handleClose}
              handleOpen={handleOpen}
              obj={bookObj}
              handleInputChange={handleInputChange}
              handleAvailableToggle={handleAvailableToggle}
              handleSubmit={handleSubmit}
              isEditing={isEditing}
              error={error}
            />
            <DetailsModal
              open={openModal}
              handleClose={handleModalClose}
              objToView={objToView}
            />
            <div className="books-outer-container">
              <div
                className="addNewButton"
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  width: "90%",
                  margin: "auto",
                  marginTop: "20px",
                }}
              >
                <Button variant="contained" onClick={handleOpen}>
                  Add New
                </Button>
              </div>
              <div className="books-select-container">
                <div style={{ display: "flex", alignItems: "center" }}>
                  <h3>Documentaries List</h3>
                </div>
                <TextField
                  id="outlined-basic"
                  label="Search"
                  variant="outlined"
                  margin="normal"
                  style={{ width: "50%" }}
                  size="small"
                  onChange={(e) => handleFilter(e.target.value)}
                />
              </div>
              <div className="books-list-container">
                <BasicTable
                  handleView={handleView}
                  handleEdit={handleEdit}
                  handleDelete={handleDelete}
                  data={documentatiationData?.documentaionDetails?.slice(
                    page * 5 - 5,
                    page * 5
                  )}
                />
              </div>
            </div>
            <div
              style={{
                height: "30px",
                padding: "20px",
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
              }}
            >
              <Pagination
                count={Math.ceil(
                  documentatiationData?.documentaionDetails?.length / 5
                )}
                variant="outlined"
                color="primary"
                onChange={handlePageChange}
              />
            </div>
          </>
        </div>
      </div>
    </Layer>
  );
};

export default Documentaries;
