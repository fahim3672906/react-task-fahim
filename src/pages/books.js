import * as React from "react";
import Button from "@mui/material/Button";
import { TextField, Pagination } from "@mui/material";
import BasicTable from "../components/table/basicTable";
import { BooksData } from "../constants/mockData/data";
import InputModal from "../components/utils/modal";
import { validateName } from "../helperfunctions/validations";
import DetailsModal from "../components/utils/detailsModal";
import { rootContext } from "../context";

const Books = () => {
  const newBooksData = React.useContext(rootContext);
  const [booksData, setBooksData] = React.useState(BooksData);
  const [open, setOpen] = React.useState(false);
  const [openModal, setOpenModal] = React.useState(false);
  const [isEditing, setIsEditing] = React.useState(false);
  const [bookObj, setBookObj] = React.useState({});
  const [objToView, setObjToView] = React.useState({});
  const [page, setPage] = React.useState(1);

  const [error, setError] = React.useState({
    title: false,
    author: false,
    director: false,
    publishedBy: false,
    language: false,
    country: false,
    year: false,
  });

  /* Filter handling */

  const handleFilter = (e) => {
    if (e) {
      const dataForBooks = booksData?.filter((book) =>
        book?.title
          .toString()
          ?.toLowerCase()
          ?.startsWith(e?.toString()?.toLowerCase())
      );
      // setBooksData(dataForBooks);
      newBooksData.setBooksDetails(dataForBooks);

      setPage(1);
    } else {
      setBooksData(BooksData);
    }
  };

  /* Form handling */
  const handleInputChange = (name, value) => {
    const data = validateName(value);
    setError((prevState) => ({
      ...prevState,
      [name]: data ? true : false,
    }));
    setBookObj((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = () => {
    if (!isEditing) {
      bookObj.id = booksData.length + 1;
      const books = [...booksData, bookObj];
      const err = Object.values(error);

      if (err.every((er) => er == false)) {
        setBooksData(books);
        newBooksData.setBooksDetails(books);

        setBookObj({});
        setOpen(false);
      } else {
        return null;
      }
    } else {
      const books = booksData?.map((book) =>
        book.id === bookObj.id ? bookObj : book
      );
      const err = Object.values(error);

      if (err.every((er) => er == false)) {
        setBooksData(books);
        newBooksData.setBooksDetails(books);
        setBookObj({});
        setOpen(false);
      } else {
        return null;
      }
    }
  };

  const handleEdit = (id) => {
    setOpen(true);
    setIsEditing(true);
    const obj = booksData?.filter((bookObj) => bookObj.id === id);
    setBookObj(obj[0]);
  };

  const handleAvailableToggle = (e) =>
    setBookObj((prevState) => ({
      ...prevState,
      availableForBorrowing: !bookObj.availableForBorrowing,
    }));

  const handleDelete = (id) => {
    const newBooks = booksData?.filter((book) => book?.id !== id);
    setBooksData(newBooks);
    newBooksData.setBooksDetails(newBooks);
  };

  /* Modal handling */
  const handleView = (id) => {
    const obj = booksData?.filter((bookObj) => bookObj.id === id);
    setObjToView(obj[0]);
    setOpenModal(true);
  };

  const handleModalClose = () => {
    setOpenModal(false);
  };

  const handleOpen = () => {
    setIsEditing(false);
    setBookObj({});
    setError({});
    setOpen(!open);
  };

  const handleClose = () => {
    setOpen(!open);
    setIsEditing(false);
  };

  /* Pagination handling */
  const handlePageChange = (event, value) => {
    setPage(value);
  };

  return (
    <>
      <InputModal
        modalTitle={"Add new Book"}
        open={open}
        handleClose={handleClose}
        handleOpen={handleOpen}
        obj={bookObj}
        handleInputChange={handleInputChange}
        handleAvailableToggle={handleAvailableToggle}
        handleSubmit={handleSubmit}
        isEditing={isEditing}
        error={error}
      />
      <DetailsModal
        open={openModal}
        handleClose={handleModalClose}
        objToView={objToView}
      />
      <div className="books-outer-container">
        <div
          className="addNewButton"
          style={{
            display: "flex",
            justifyContent: "flex-end",
            width: "90%",
            margin: "auto",
            marginTop: "20px",
          }}
        >
          <Button variant="contained" onClick={handleOpen}>
            Add New
          </Button>
        </div>
        <div className="books-select-container">
          <div style={{ display: "flex", alignItems: "center" }}>
            <h3>Books List</h3>
          </div>
          <TextField
            id="outlined-basic"
            label="Search"
            variant="outlined"
            margin="normal"
            style={{ width: "50%" }}
            size="small"
            onChange={(e) => handleFilter(e.target.value)}
          />
        </div>
        <div className="books-list-container">
          <BasicTable
            handleView={handleView}
            handleEdit={handleEdit}
            handleDelete={handleDelete}
            data={newBooksData?.booksDetails.slice(page * 5 - 5, page * 5)}
          />
        </div>
      </div>
      <div
        style={{
          height: "30px",
          padding: "20px",
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <Pagination
          count={Math.ceil(newBooksData?.booksDetails?.length / 5)}
          variant="outlined"
          color="primary"
          onChange={handlePageChange}
        />
      </div>
    </>
  );
};

export default Books;
