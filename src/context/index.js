import React, { createContext } from "react";

export const rootContext = createContext({
  data: "",
  setData: () => {},
});
