export const BooksData = [
  {
    id: 1,
    author: "Honor\u00e9 de Balzac",
    country: "France",
    year: "2015",
    language: "French",
    availableForBorrowing: false,
    title: "Le P\u00e8re Goriot",
  },
  {
    id: 2,
    author: "Samuel Beckett",
    country: "Republic of Ireland",
    language: "French, English",
    year: "2014",
    availableForBorrowing: false,
    title: "Molloy, Malone Dies, The Unnamable, the trilogy",
  },
  {
    id: 3,
    author: "Giovanni Boccaccio",
    country: "Italy",
    language: "Italian",
    availableForBorrowing: false,
    title: "The Decameron",
  },
  {
    id: 4,
    author: "Jorge Luis Borges",
    country: "Argentina",
    language: "Spanish",
    availableForBorrowing: false,
    title: "Ficciones",
  },
  {
    id: 5,
    author: "Emily Bront\u00eb",
    country: "United Kingdom",
    language: "English",
    availableForBorrowing: false,
    title: "Wuthering Heights",
  },
  {
    id: 6,
    author: "Albert Camus",
    country: "Algeria, French Empire",
    language: "French",
    availableForBorrowing: false,
    title: "The Stranger",
  },
  {
    id: 7,
    author: "Paul Celan",
    country: "Romania, France",
    language: "German",
    availableForBorrowing: false,
    title: "Poems",
  },
  {
    id: 8,
    author: "Louis-Ferdinand C\u00e9line",
    country: "France",
    language: "French",
    availableForBorrowing: false,
    title: "Journey to the End of the Night",
  },
];

export const DocumentariesData = [
  {
    id: 1,
    title: "Beetlejuice",
    year: "1988",
    country: "England",
    language: "English",
    availableForBorrowing: false,
    director: "Geoffrey Chaucer",
  },
  {
    id: 2,
    title: "The Cotton Club",
    country: "England",
    language: "English",
    year: "1984",
    availableForBorrowing: false,
    director: "Miguel de Cervantes"
  },
  {
    id: 3,
    title: "The Shawshank Redemption",
    country: "England",
    language: "English",
    year: "1999",
    availableForBorrowing: false,
    director: "Frank Darabont"
  },
  {
    id: 4,
    title: "Crocodile Dundee",
    country: "England",
    language: "English",
    year: "1986",
    availableForBorrowing: false,
    director: "Peter Faiman"
  },
  {
    id: 5,
    title: "Valkyrie",
    country: "England",
    language: "English",
    year: "2008",
    availableForBorrowing: false,
    director: "Bryan Singer"
  },
  {
    id: 5,
    title: "Ratatouille",
    country: "England",
    language: "English",
    year: "2007",
    availableForBorrowing: true,
    director: "Brad Bird, Jan Pinkava"
  },
];

export const NewspapersData = [
  {
    id: 1,
    title: "The World Daily.",
    country: "England",
    language: "English",
    year: "2020",
    availableForBorrowing: false,
    publishedBy: "The Daily"
  },
  {
    id: 2,
    title: "The International Bulletin.",
    availableForBorrowing: false,
    country: "USA",
    language: "English",
    year: "2022",
    publishedBy: "The International"
  },
];
