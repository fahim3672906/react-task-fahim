export const messages = {
    isRequired: "This field is required.",
    notValid: "Input value is not Valid",
  };
    
  
  export const fieldNames = {
      title: 'title',
      author: "author",
      director: "director",
      newsContent: "newsContent",
      availableForBorrowing: "availableForBorrowing",
      language: "language",
      country: "country",
      newsContent: "newsContent",
  }