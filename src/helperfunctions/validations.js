import { messages } from '../constants/formConstants';
import {
  isNameValid,
} from './regex';

export const validateName = (fieldValue) => {
  return fieldValue.trim() === ''
    ? messages.isRequired
    : isNameValid(fieldValue)
    ? ''
    : messages.notValid;
};

export const isRequired = (fieldValue) => {
  return fieldValue.trim() === ''
    ? messages.isRequired
    : '';
};
