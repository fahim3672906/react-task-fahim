import React, { useContext } from "react";
import { Navigate, Outlet } from "react-router-dom";

const PrivateRoutes = () => {
  const userData = window.localStorage.getItem("users");
  return userData ? <Outlet /> : <Navigate to="/signin" />;
};

export default PrivateRoutes;
