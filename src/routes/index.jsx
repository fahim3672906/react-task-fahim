import React, { useEffect } from "react";
import { useState } from "react";

import ReactDOM from "react-dom";

import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import Library from "../components/library/library";
import SignIn from "../login/signin";
import SignUp from "../login/signup";
import Dashboard from "../pages/dashboard";
import Documentaries from "../pages/documentaries";
import Newspapers from "../pages/newspapers";
import NotFound from "../pages/notFound";
import PrivateRoutes from "./privateRoutes";

const RoutesProvider = () => {
  function Logged({ children }) {
    const usersData = JSON.parse(window.localStorage.getItem("loggedInUser"));
    return usersData ? <Navigate to="/books" replace /> : children;
  }

  function RequireAuth({ children }) {
    const usersData = JSON.parse(window.localStorage.getItem("loggedInUser"));
    return usersData ? children : <Navigate to="/signin" replace />;
  }

  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <RequireAuth>
              <PrivateRoutes />
            </RequireAuth>
          }
        >
          <Route path="/" element={<Dashboard />} />
          <Route path="/books" element={<Library />} />
          <Route path="/documentaries" element={<Documentaries />} />
          <Route path="/newspapers" element={<Newspapers />} />
          <Route path="*" element={<NotFound />} />
        </Route>

        <Route
          path="/signin"
          element={
            <Logged>
              <SignIn />
            </Logged>
          }
        />
        <Route
          path="/signup"
          element={
            <Logged>
              <SignUp />
            </Logged>
          }
        />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default RoutesProvider;
